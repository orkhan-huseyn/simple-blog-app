import { Link } from 'react-router-dom';

function Header() {
  return (
    <header>
      <nav className="navbar bg-light">
        <div className="container">
          <Link to="/" className="navbar-brand">
            Blog App
          </Link>
          <Link className="nav-link me-auto" to="/chat">
            Chat
          </Link>
          <form className="d-flex" role="search">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search blog"
              aria-label="Search blog"
            />
            <button className="btn btn-outline-success" type="submit">
              Search
            </button>
          </form>
        </div>
      </nav>
    </header>
  );
}

export default Header;
