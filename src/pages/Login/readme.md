# How to login

On login page:

```js
import {useNavigate} from 'react-router-dom';

...

const navigate = useNavigate();

...

axios.post('/authorize').then((response) => {
    const userInfo = response.data;
    localStorage.setItem('user_info', JSON.stringify(userInfo));
    navigate('/');
});
```

On other pages:

```js

function Home() {
    const [nese, setNese] = useState();

    ...
    const userInfo = localStorage.getItem('user_info');
    if (!userInfo) {
        return <Navigate to="/login"/>
    }
    ...

}

```
