import React from 'react';

const LazyChatPage = React.lazy(() => import('./Chat'));

const WithSuspense = () => (
  <React.Suspense fallback={'Loading chat page ...'}>
    <LazyChatPage />
  </React.Suspense>
);

export default WithSuspense;
