import { useEffect, useRef, useState } from 'react';
import { getRandomColor } from '../../utils/colors';

const WS_URL = 'ws://localhost:8080';

function Chat() {
  const socketRef = useRef();
  const listRef = useRef();
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const height = listRef.current.scrollHeight;
    listRef.current.scrollTo({
      top: height,
    });
  }, [messages]);

  useEffect(() => {
    socketRef.current = new WebSocket(WS_URL);

    socketRef.current.onmessage = function (event) {
      const message = {
        ...JSON.parse(event.data),
        color: getRandomColor(),
      };
      setMessages((messages) => [...messages, message]);
    };

    socketRef.current.onerror = () => {};

    return () => {
      socketRef.current.close();
    };
  }, []);

  function handleSubmit(event) {
    event.preventDefault();
    const { messageInput } = event.target.elements;
    socketRef.current.send(messageInput.value);
    setTimeout(() => (messageInput.value = ''));
  }

  return (
    <>
      <h1>Hello from Chat page!</h1>
      <ul ref={listRef} className="message-box">
        {messages.map((message, index) => {
          const { color } = message;

          if (message.type === 'chat-message') {
            return (
              <li key={index}>
                <span style={{ color }}>{message.userName}</span>
                {message.text}
              </li>
            );
          }

          if (message.type === 'join') {
            return (
              <li key={index}>
                <span style={{ color }}>{message.userName}</span>
                joined to chat
              </li>
            );
          }

          return (
            <li key={index}>
              <span style={{ color }}>{message.userName}</span> banned
            </li>
          );
        })}
      </ul>
      <form className="d-flex mt-2" autoComplete="off" onSubmit={handleSubmit}>
        <input
          className="form-control me-2"
          name="messageInput"
          placeholder="Message..."
        />
        <button className="btn btn-primary">Send</button>
      </form>
    </>
  );
}

export default Chat;
