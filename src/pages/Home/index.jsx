import React from 'react';

const LazyHomePage = React.lazy(() => import('./Home'));

const WithSuspense = () => (
  <React.Suspense fallback={'Loading home page ...'}>
    <LazyHomePage />
  </React.Suspense>
);

export default WithSuspense;
