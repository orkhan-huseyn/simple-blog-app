import { Link } from 'react-router-dom';
import { usePosts } from '../../hooks/usePosts';

function Home() {
  const [posts, loading, error, loadMore, hasMore] = usePosts();

  return (
    <>
      {error && (
        <div className="alert alert-danger" role="alert">
          {error}
        </div>
      )}
      {loading && (
        <div className="d-flex justify-content-center align-items-center py-3">
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      )}
      <ul className="list-group">
        {posts.map((post) => (
          <li
            key={post.id}
            className="list-group-item d-flex justify-content-between align-items-start"
          >
            <div className="ms-2 me-auto">
              <Link to={`/${post.id}`} className="d-block fw-bold text-decoration-none text-black">
                {post.title}
              </Link>
              {post.body}
              <div className="my-2">
                {post.tags.map((tag) => (
                  <span key={tag} className="badge text-bg-secondary me-1">
                    {tag}
                  </span>
                ))}
              </div>
            </div>
            <span className="badge bg-dark rounded-pill">
              {post.reactions}
            </span>
          </li>
        ))}
      </ul>
      {hasMore && (
        <div className="text-center my-3">
          <button
            disabled={loading}
            onClick={loadMore}
            className="btn btn-primary"
          >
            {loading ? 'Loading more...' : 'Load more'}
          </button>
        </div>
      )}
    </>
  );
}

export default Home;
