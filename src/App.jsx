import { Routes, Route } from 'react-router-dom';

import DefaultLayout from './layouts/DefaultLayout';
import HomePage from './pages/Home';
import ChatPage from './pages/Chat';

function App() {
  return (
    <Routes>
      <Route path="/" element={<DefaultLayout />}>
        <Route path="" element={<HomePage />} />
        <Route path="/:postId" element={<h1>Blog page</h1>} />
        <Route path="/create" element={<h1>Create blog page</h1>} />
        <Route path="/chat" element={<ChatPage />} />
      </Route>
      <Route path="/login" element={<h1>Login page</h1>} />
    </Routes>
  );
}

export default App;
