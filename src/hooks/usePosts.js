import { useEffect, useState } from 'react';
import axios from '../axios';

const LIMIT = 5;

export function usePosts() {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [total, setTotal] = useState(0);
  const [skip, setSkip] = useState(0);
  const [error, setError] = useState('');

  useEffect(() => {
    setLoading(true);
    setError('');
    axios
      .get(`/posts?limit=${LIMIT}&skip=${skip}`)
      .then((response) => {
        const { posts, total } = response.data;
        setPosts((oldPosts) => [...oldPosts, ...posts]);
        setTotal(total);
      })
      .catch((error) => {
        setError(error.message);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [skip]);

  function loadMore() {
    setSkip(skip + LIMIT);
  }

  const hasMore = posts.length < total;

  return [posts, loading, error, loadMore, hasMore];
}
