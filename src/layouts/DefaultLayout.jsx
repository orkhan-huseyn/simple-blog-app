import { Outlet } from 'react-router-dom';
import Header from '../components/Header';

function DefaultLayout() {
  return (
    <>
      <Header />
      <main className="my-4 container">
        <Outlet />
      </main>
    </>
  );
}

export default DefaultLayout;
