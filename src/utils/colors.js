export function getRandomColor() {
  const randomHex = Math.floor(Math.random() * 0xffffff);
  const hexString = randomHex.toString(16);
  return '#' + hexString;
}
